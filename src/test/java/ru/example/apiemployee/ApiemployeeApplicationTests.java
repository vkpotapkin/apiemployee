package ru.example.apiemployee;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import ru.example.ApiClient;
import ru.example.client.DefaultApi;
import ru.example.model.EmployeeItem;

import java.util.List;

@SpringBootTest
class ApiemployeeApplicationTests {

	@Test
	@Disabled
	void getEmployeeClientTest(){
		ApiClient apiClient = new ApiClient();
		apiClient.setBasePath("http://localhost:8080/");
		DefaultApi client = new DefaultApi();
		client.setApiClient(apiClient);
		List<EmployeeItem> list = client.getEmployee(10);
		System.out.println(list);
	}
}
