package ru.example.apiemployee.controller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.example.controller.EmployeeApi;
import ru.example.model.EmployeeItem;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@EnableAutoConfiguration
public class EmployeeController implements EmployeeApi {
    @Override
    public ResponseEntity<List<EmployeeItem>> getEmployee(Integer maxrecord) {
        List<EmployeeItem> employeeList =  new ArrayList<EmployeeItem>();
        EmployeeItem item = new EmployeeItem();
        item.setId(UUID.randomUUID());
        item.setName("Vadim");
        item.setSalary(1000);
        item.setDateOfBirth(LocalDate.of(2000, 01,01));
        employeeList.add(item);
        ResponseEntity<List<EmployeeItem>> responseEntity = new ResponseEntity(employeeList, HttpStatus.OK);
        return responseEntity;
    }
}
