package ru.example.apiemployee.controller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ThreadLocalRandom;

@RestController
@EnableAutoConfiguration
public class TestController {

    @GetMapping(value = "/testController")
    @ResponseBody
    public String getInternationalContent (){
        int randomNum = ThreadLocalRandom.current().nextInt(0, 100 + 1);
        String response = "Server Response text message + random 0-100 = " + randomNum;
        return response;
    }
}
